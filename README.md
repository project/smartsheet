# Smartsheet

The Smartsheet module provides a service to easily interact with Smartsheet's REST API. The module also support submission of form data directly to Smartsheet for developers.

This module only supports the 2.0 version of the Smartsheet API.

## Installation

Install as you would normally install a contributed Drupal module. See [Installing Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.

## Configuration

A valid Smartsheet access token is required for the module to work. It can be generated within the [Smartsheet webapp](https://app.smartsheet.com/b/home).
With the previously generated token, the access_token configuration key needs to be set in the smartsheet.config namespace. It can be done by two different ways:

### Settings File
In the settings.php file, like this: $config['smartsheet.config']['access_token'] = <GENERATED TOKEN>

### Admin UI
A configuration interface is available at "Administration » Configuration » Web services » Smartsheet API (admin/config/services/smartsheet).

## Usage

### API

The module provides the smartsheet.client service that interfaces directly the Smartsheet's REST API. This service offers these methods:

  ```
  $instance->get($path, $options); // GET request
  $instance->post($path, $data, $options); // POST request
  $instance->put($path, $data, $options); // PUT request
  $instance->delete($path, $options); // DELETE request
  ```

The $data array is the body of the POST or PUT request to send to the Smartsheet API.
The $options array is passed directly to the Guzzle request. Use it for example to add query string to the URL.

For further documentation about the Smartsheet REST API got to [Smartsheet API Docs](https://smartsheet-platform.github.io/api-docs/)

### Forms support

This module also offers the possibility to add values submitted by a custom form (built with Form API) as a new row to a sheet. To use this feature, add the following properties to your form:

* `$form['#smartsheet_sheet_id'] = <SMARTSHEET ID>;` Where <SMARTSHEET ID> is the ID of the sheet inside which the values are to be inserted.
* `$form['#smartsheet_column_mapping'] = <FIELD MAPPING>;` Where <FIELD MAPPING> is an associative array whose keys are form field names and values are the matching column title of ids from the sheet.

Each mapped value submitted in the form will be inserted in the matching column. For example:

```
$form['#smartsheet_column_mapping'] = [
 'firstname' => 'First Name';
 'lastname' => 'Last Name';
 'email' => 4074863598216325;
];
```
